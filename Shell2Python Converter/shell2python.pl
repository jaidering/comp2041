#!/usr/bin/perl
use strict;
use warnings;
use Text::ParseWords;

# function declarations
sub convertToPython();
sub quoteIfNotQuoted();
sub addToImports();
sub makeList();
sub buildTestOperators();
sub stringToPythonFormat();
sub buildNumericalOperators();
sub testToPythonOperator();
sub isNumeric();
sub handleTest();
sub postProcess();

# global variables
my @processedLines;

my $recurseDepth = 0;
my $intendentDepth = 0;

my $inThisCaseStatement = 0;
my $inThisCaseDepth = 0;

my %imports;
my %testOperators;
my %listSpecialCases;
my %numericalOperators;
my %pythonReservedWords;
my %shellSpecialVariables;

my $secondLine;

# setup-functions
&buildNumericalOperators();
&buildTestOperators();
&buildListSpecialCases();
&buildPythonReservedWords();
&buildshellSpecialVariables();

# process command line arguments as files
if (@ARGV) {

   for my $file (@ARGV) {

      if (open (FILE, "< $file")) {
      
         while (my $line = <FILE>) {
            push(@processedLines, &convertToPython ($line));
         }
         
         &postProcess();
         
      } else {
      
         print (STDERR "Could not open file: $file : $!" . "\n");
      }
   }
}

# filter functionality
else {

   while (my $line = <>) {
      push(@processedLines, &convertToPython ($line));
   }
   
   &postProcess();
}

sub convertToPython () {
   my $lastLineWasIntendent = 0;
   my $pauseIntendent = 0;
   my $comment;
   my ($line) = @_;
   
   # remove whitespace at front+end of line
   $line =~ s/^\s+//g;
   $line =~ s/\s+$//g;
   
   # if it's a blank line
   if (!length($line)) {
      $line = "\n";
   }
   
   # find trailing comments
   if ($line =~ /(.+)([^\$]#+.*)$/i) {
      $line = $1;
      $comment = $2;
   }

   # command or variable assignment, not a case statement
   if ($line =~ /(^[\.\/a-z]+[\.\/a-z0-9_]*)\s*(.*)\s*/i && $line !~ /^.+\).*/i) {
   
      my $command = $1;
      my $argumentString = $2;
      my (@arguments) = &shellwords($argumentString);
      

      # find multiexpression lines (;)
      # TODO make it actually work
      my $i = 0;
      for my $arg (@arguments) {
      
         # TODO expand for &&, || and | as well
         # http://docs.python.org/library/subprocess.html#replacing-shell-pipeline
      
         if ($arg eq ";" || $arg =~ /(.*);$/) {
         
            my $subExpression = join(" ", @arguments[$i+1, $#arguments]);
            delete @arguments[$i .. $#arguments];
            
            $recurseDepth++;
            $secondLine = &convertToPython($subExpression);
            $recurseDepth--;
            
            last;
         }
         
         elsif ($arg eq "<") {
         
            &addToImports ("from subprocess import Popen, PIPE, STDOUT");
         
            my $readFrom = $arguments[$i+1];
            delete @arguments[$i .. $#arguments];
            
            unshift (@arguments, $command);
            
            
            $line = "\n" .
                    "readFileHandle$$ = open(". &stringToPythonFormat($readFrom) .", 'r').read()" . "\n" .
                    "p = Popen(". &makeList(@arguments) .", stdout=PIPE, stdin=PIPE, stderr=STDOUT)" . "\n" .
                    "pStdout = p.communicate(input=readFileHandle$$)[0]" . "\n" .
                    "print str(pStdout).rstrip('\\n')" . "\n";
                    
            return $line;
         }
         
         $i++;
      }
      
      # is this a variable assignment
      if ($argumentString =~ /^=(.+)$/) {

         my $variable = &stringToPythonFormat('$'.$command);
         my $toAssign = $1;

         # if not a string literal
         if ($toAssign !~ /^'[^']'$/) {

            # if a `command xxx`
            if ($toAssign =~ /`[^`]+`/) {
               
               $toAssign = &stringToPythonFormat($toAssign);
               
            } else {
            
               # if it's quoted, remove the quotes
               if ($toAssign =~ /^"[^"]+"$/) {
                  $toAssign =~ s/^"//;
                  $toAssign =~ s/"$//;
               }
            
               my (@assignments) = &shellwords($toAssign);
               foreach my $assignment (@assignments) {
                  $assignment = &transformshellSpecialVariables (&stringToPythonFormat ($assignment));
               }
               
               $toAssign = join (" + ' ' + ", @assignments);
            }
         }
         
         # if we're assigning from a subprocess call
         if ($toAssign =~ /^subprocess\.call\(\[(.+)\]\)$/) {
         
            $line = "p$$ = subprocess.Popen([$1], stdout=subprocess.PIPE)" . "\n" . "$variable = str(p$$.communicate()[0]).rstrip('\\n')";
            
         } else {
      
            $line = $variable . " = " . $toAssign;
         }
      }
      
      # this is not a variable assignment
      else {

         if ($command eq "echo") {

            # if it's a literal quoted ' string '
            if ($argumentString =~ /^('[^']+')$/) {
               $line = "print " . ($argumentString);
            }
            
            # if we're outputting to a file
            elsif ((
                     $argumentString =~ /\s*(\$?[a-z]+[\w]*)\s*([>]+)\s*(.+)\S*/i ||
                     $argumentString =~ /\s*(".*")\s*([><]+)\s*(.+)\S*/i
                   )
                  && $argumentString !~ /^"[^"]+"$/) {
               
               my $openType;
               if ($2 eq ">>") {
                  $openType = 'a';
               } elsif ($2 eq ">") {
                  $openType = 'w';
               } else {
                  $openType = 'r'; # default to 'r', for safety?
               }

               my $fileToOpen = &stringToPythonFormat($3);
               my $fileHandleName = 'fileHandle'.$$;
               $line = "with open($fileToOpen, '$openType') as ".$fileHandleName.": print >> ".$fileHandleName.", ".&stringToPythonFormat($1);
            }
            
            # just doing a normal echo
            else {
            
               my $noNewLine = 0;
               # if it's quoted, remove the quotes
               if ($argumentString =~ /^"[^"]+"$/) {
                  $argumentString =~ s/^"//;
                  $argumentString =~ s/"$//;
                  
                  @arguments = &shellwords($argumentString);
                  
               } elsif ($argumentString =~ /""/) {
               
                  $argumentString = ""
               
               } elsif (@arguments && $arguments[0] eq "-n") {
               
                  shift(@arguments);
                  $noNewLine = 1;
               }

               foreach my $argument (@arguments) {
                  $argument = &stringToPythonFormat ($argument);
               }

               $argumentString = join (", ", @arguments);
               
               if (($argumentString =~ /^'[^']+'$/ || $argumentString =~ /[a-z_]+[0-9]*/) && $noNewLine) {
               
                  &addToImports ("import sys");
                  $line = "sys.stdout.write($argumentString)"
               
               } else {
               
                  $line = "print " . ($argumentString);
               }
               
               if ($line =~ /(.*)(sys\.argv\[1:\])(.*)/) {
               
                  $line = $1 . "' '.join(" . $2 . ")" . $3;
               }
            }
         }
         
         elsif ($command eq "if" || $command eq "elif" || $command eq "while") {
            
            if ($command ne "elif") {
               $intendentDepth++;
            }
            
            $pauseIntendent = 1;
            
            $line = $command . " ";
            
            my $condition = shift(@arguments);
            
            if ($condition eq "test") {
               
               my $var1;
               my $operator;
               
               my $negation = 0;
                  
               if (@arguments && $arguments[0] eq "!") {
                  $negation = 1;
                  shift (@arguments);
               }
               
               if ($negation) {
                  $line .= "not ";
               }
               
               # if we have test -xxx var1
               if (@arguments && $arguments[0] =~ /\-[a-z]+/i) {
               
                  $operator = &testToPythonOperator(shift (@arguments));
                  $var1 = &stringToPythonFormat(shift (@arguments));
                  
                  if ($operator eq "R_OK") {
                  
                     &addToImports ("import os");
                     $line .= "os.access($var1, os.$operator)";
                  }
                  
                  elsif ($operator eq "isFile") {
                     
                     &addToImports ("import os.path");
                     $line .= "os.path.exists($var1)";
                  }
                  
                  elsif ($operator eq "isDirectory") {
                     
                     &addToImports ("import os.path");
                     $line .= "os.path.isdir($var1)";
                  }
               }
               
               # if we have test var1 xx var2
               else {
               
                  while (@arguments) {
                     $line .= &handleTest(\@arguments);

                     if (@arguments && &testToPythonOperator ($arguments[0])) {
                        $line .= " " . &testToPythonOperator (shift (@arguments)) . " ";
                        $line .= &handleTest(\@arguments);
                     }
                  }
               }
            }
            
            elsif ($condition eq "true") {
            
               &addToImports ("import subprocess");
               $line .= "subprocess.call(['false'])";
            }
            
            elsif ($condition eq "false") {
            
               &addToImports ("import subprocess");
               $line .= "subprocess.call(['true'])";
            }
            
            else {
            
               $recurseDepth++;
               $line .= "not " . &convertToPython ($argumentString);
               $recurseDepth--;
            }
            
            $line .= ":";
         }
         
         elsif ($command eq "for") {
            
            my $inWhat;
            my $variableName = &stringToPythonFormat("\$" . shift(@arguments));
            shift (@arguments); # in
            
            if (@arguments && $arguments[0] =~ /(.*)(\*.*)/) {
            
               &addToImports ("import glob");
               
               $inWhat = 'sorted(glob.glob("' . $arguments[0] . '"))';
            
            } else {
            
               $inWhat = &makeList (@arguments);
            }
            
            $line = $command . " " .
                    $variableName . " in " .
                    $inWhat . ":";
                    
            $intendentDepth++;   
            $pauseIntendent = 1;
         }
         
         elsif ($command eq "sleep") {
            &addToImports("import time");
            $line = "time.sleep(". &stringToPythonFormat($arguments[0]) .")"
         }
         
         elsif ($command eq "case") {
            $inThisCaseStatement = shift (@arguments);
            return;
         }
         
         elsif ($command eq "esac") {
            $inThisCaseStatement = 0;
            $inThisCaseDepth = 0;
            $intendentDepth--;
            return;
         }
         
         elsif ($command eq "read") {
            &addToImports ("import sys");
            $line = $argumentString . " = sys.stdin.readline().rstrip()";
         }
         
         elsif ($command eq "cd") {
            &addToImports ("import os");
            $line = "os.chdir(" . &stringToPythonFormat($argumentString) . ")";
         }
         
         elsif ($command eq "exit") {
            &addToImports ("import sys");
            $line = "sys.exit(" . $argumentString . ")";
         }
         
         elsif ($command eq "else") {
            $pauseIntendent = 1;
            $line = "else:";
         }
         
         elsif ($command eq "expr") {
            $recurseDepth++;
            $line = &convertToPython ($argumentString);
            $recurseDepth--;
         }
         
         elsif ($command eq "let") {
            $recurseDepth++;
            $line = &convertToPython ($argumentString);
            $recurseDepth--;
         }
         
         elsif ($command eq "do") {
            return;
         }
         
         elsif ($command eq "then") {
            return;
         }
         
         elsif ($command eq "done" || $command eq "fi") {
            $intendentDepth--;
            return;
         }
         
         # catch-all for commands without specific instructions
         else {

            my $commandList = &makeList(&shellwords ($line));
            
            &addToImports ("import subprocess");

            $line = 'subprocess.call('. $commandList .')';
         }
      }
   }
   
   # math.. probably.
   elsif ($line =~ /^(\$*)([a-z0-9_]+)\s*['"]*\s*([\+\-\/\*\%\=]+)\s*['"]*\s*(\$*)([a-z0-9_]+)\s*(.*)/i) {
   
      my $var1 = $2;
      my $operator = $3;
      my $var2 = $5;
      my $rest = $6;
      
      $line = "int(".$var1.") " .
              $operator . " " .
              "int(".$var2.")" .
              ($rest ? " ".$rest : "");
   }
   
   # case statements: cases
   elsif ($line =~ /^(.+)\)\s*(.*)\s*;;/i && $inThisCaseStatement) {
   
      my $case = $1;
      my $instruction = $2;
      
      if (!$inThisCaseDepth) {
      
         $line = "if ";
         
      } else {
      
         if ($case eq "*") {
            $line = "else ";
         } else {
            $line = "elif ";
         }
      }
      
      $pauseIntendent = 1;
      $recurseDepth++;
      $line = &convertToPython ($line . " test $inThisCaseStatement = $case");
      $recurseDepth--;
      
      $line .= "\n";
      
      $line .= &convertToPython ($instruction);
      
      $inThisCaseDepth++;
    }
    
    # trying to execute a variable? fine.
    elsif ($line =~ /\$[a-z0-9_]+/) {
    
      &addToImports ("import subprocess");
      
      $line = 'subprocess.call('. &stringToPythonFormat ($line) .')';
    }
    
    # If we can't translate it, comment it out.
    elsif ($line =~ /^[^\#^\s]+.*/i) {
    
      $line = "# FIXME: could not be translated # " . $line;
    }
   
   # add indentation
   if ($intendentDepth && !$recurseDepth) {
   
      my $intendentTo = ($pauseIntendent ? $intendentDepth-1 : $intendentDepth);
      
      while ($intendentTo > 0) {
         $line = "    ". $line;
         $intendentTo--;
      }
   }
   
   # add back the comment
   if ($comment) {
      $line .= " " . $comment;
   }
   
   # if we have a non-blank line, add a linebreak
   if ($line =~ /\S+/gi && !$recurseDepth) {
      $line .= "\n";
   }
   
   if ($secondLine) {
      $line .= "\n" . $secondLine;
   }

   return $line;
}

sub quoteIfNotQuoted () {
   my ($string) = @_;
   
   # Unquoted
   if ($string !~ /^("|').*("|')$/ && $string !~ /^\$.+$/) {
   
      $string =~ s/"/\"/g;
      $string =~ s/'/\\'/g;
      $string = "'". $string . "'";
   }

   return $string;
}

sub makeList () {

   my @elements = @_;
   
   my $i = 0;
   my $list;
   my @specialCases;
   foreach my $element (@elements) {
   
      $element = &stringToPythonFormat($element);
      
      if ($listSpecialCases{$element}) {
         push (@specialCases, splice(@elements, $i));
      }
      
      if ($element =~ /^[a-z]+[0-9_]*$/i) {
         $element = "str($element)";
      }
      
      $i++;
   }
   
   if (@elements) {
      $list = join (", ", @elements);
      $list = "[". $list ."]";
      
      if (@specialCases) {
         $list .= "+";
      }
   }
   
   $list .= join('+', @specialCases);
   
   return $list;
}

sub addToImports () {

   my ($import) = @_;

   return $imports{$import}++;
}

sub stringToPythonFormat () {
   
   my($argument) = @_;
   
   # $var1
   if ($argument =~ /^"*(\$)([a-z]+[a-z0-9_]*)"*/i) {
      if ($pythonReservedWords{$2}) {
         $argument = $2.$$;
      } else {
         $argument = $2;
      }
   }
   
   # $[0-9]
   elsif ($argument =~ /^"*([a-z0-9_\.\/]*)(\$)([0-9]+)([a-z0-9_\.\/]*)"*/i) {
   
      &addToImports ("import sys");
      $argument = ($1 ? "'" . $1 . "' + " : "") . "sys.argv[". $3 . "]" . ($4 ? " + '" . $4 . "'" : "");
   }
   
   # $#
   elsif ($argument =~ /^"*([a-z0-9_\.\/]*)\$\#([a-z0-9_\.\/]*)"*/i) {
   
      &addToImports ("import sys");
      $argument = ($1 ? "'" . $1 . "' + str(" : "") . "int(len(sys.argv)-1)" . ($2 ? " + '" . $2 . "'" : "") . ($1 ? ")" : "");
   }
   
   # $$
   elsif ($argument =~ /^"*([a-z0-9_\.\/]*)\$\$([a-z0-9_\.\/]*)"*/i) {
   
      &addToImports ("import os");
      #$argument = ($1 ? "'" . $1 . "' + str(" : "") . "os.getpid()" . ($2 ? " + '" . $2 . "'" : "");
      $argument = ($1 ? "'" . $1 . "' + str(" : "") . "int(os.getpid())" . ($2 ? " + '" . $2 . "'" : "") . ($1 ? ")" : "");
   }
   
   # $@
   elsif ($argument =~ /^(\S*)\$[\@\*]+(\S*)/i) {
   
      &addToImports ("import sys");
      $argument = "sys.argv[1:]";
   }
   
   # $*
   elsif ($argument =~ /^(\S*)\$[\*]+(\S*)/i) {
   
      &addToImports ("import sys");
      $argument = "sys.argv[1:]";
   }
   
   # ` instructions `
   elsif ($argument =~ /^(`)([^`]+)(`)/i) {
      $recurseDepth++;
      $argument = &convertToPython ($2);
      $recurseDepth--;
   }
   
   # "quoted string"
   elsif ($argument =~ /^"([^"]+)"/i) {
   
      #$2 =~ s/(\$)([a-z]+[a-z0-9_])*/$2/gi;
      $argument = "'". $1 . "'";
   }
   
   elsif (&isNumeric($argument)) {
      # do something?
   }
   
   # just quote it
   else {
   
      $argument =~ s/'/\\'/g;
      $argument = "'".$argument."'";
   }
   
   return $argument;
}

sub testToPythonOperator () {

   my($operator) = @_;
   
   return $testOperators{$operator};
}

sub buildTestOperators () {

   %testOperators = (%testOperators, %numericalOperators);
   
   $testOperators{"="} = "==";
   $testOperators{"!="} = "!=";
   $testOperators{"-o"} = "or";
   $testOperators{"-a"} = "and";
   $testOperators{"-r"} = "R_OK";
   $testOperators{"-d"} = "isDirectory";
   $testOperators{"-e"} = "isFile";
   $testOperators{"-f"} = "isFile";
}

sub buildNumericalOperators () {

   $numericalOperators{"-eq"} = "==";
   $numericalOperators{"-ge"} = ">=";
   $numericalOperators{"-gt"} = ">";
   $numericalOperators{"-le"} = "<=";
   $numericalOperators{"-lt"} = "<";
   $numericalOperators{"-ne"} = "!=";
}

sub buildPythonReservedWords () {

   $pythonReservedWords{"print"}++;
   $pythonReservedWords{"import"}++;
   $pythonReservedWords{"use"}++;
   $pythonReservedWords{"def"}++;
   $pythonReservedWords{"return"}++;
}

sub buildshellSpecialVariables () {

   $shellSpecialVariables{"RANDOM"} = "random.randint(0, 32767)";
}

sub transformshellSpecialVariables () {
   my($variable) = @_;

   if ($shellSpecialVariables{$variable}) {
      
      if ($variable eq "RANDOM") {
         &addToImports("import random");
      }
      
      $variable = $shellSpecialVariables{$variable};
   }

   return $variable;
}

sub buildListSpecialCases () {

   $listSpecialCases{"sys.argv[1:]"}++;
}

sub isNumeric () {

   my($val) = @_;
   
   if ($val =~ /^\-*[0-9]+$/) {
      return 1;
   } else {
      return 0;
   }
}

sub handleTest () {

   my($arguments) = @_;
   
   my $var1 = &stringToPythonFormat ( shift (@{$arguments}) );
   my $originalOperator = shift (@{$arguments});
   my $operator = &testToPythonOperator ($originalOperator);
   my $var2 = &stringToPythonFormat ( shift (@{$arguments}) );
   
   # if this is a test numerical operator, make vars ints
   if ($numericalOperators{$originalOperator}) {
      
      if ($var1 eq "sys.argv[1:]") {
         $var1 = "len($var1)";
      } else {
         $var1 = "int($var1)";
      }
      
      if ($var2 eq "sys.argv[1:]") {
         $var2 = "len($var2)";
      } else {
         $var2 = "int($var2)";
      }
   }
   
   return $var1 . " " . $operator . " " . $var2;
}

sub postProcess () {
   # remove the interpreter (assumed to be there)
   shift(@processedLines);

   # import dependencies
   for my $import (keys (%imports)) {
      unshift(@processedLines, $import . "\n");
   }

   # put interpreter up top
   unshift (@processedLines, "#!/usr/bin/python2.7 -u" . "\n");

   print @processedLines;

   # empty @lines
   @processedLines = ();
}