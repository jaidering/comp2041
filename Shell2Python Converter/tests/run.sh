#!/bin/sh

if test "$1" = "test"
then

   if test "$2" = "personal"
   then
      for file in personal/*.sh
      do
         echo "Running.. $file"
         ../shell2python.pl "$file" > "$file.my.py"
      done

   elif test -d "$2"
   then
      for file in "$2"/*.sh
      do
         echo "Running tests for $file.."
      
         echo "Converting.. $file" &&
         ../shell2python.pl "$file" > "$file.my.py" &&
         
         echo "Running.. $file.ref.py" &&
         python2.7 -u "$file".ref.py 1 5 4 5 8 > "$file".ref.out &&
         
         echo "Running.. $file.my.py" &&
         python2.7 -u "$file".my.py 1 5 4 5 8 > "$file".my.out &&
         
         echo "Comparing.. $file.ref.out to $file.my.out" &&
         diff "$file".ref.out "$file".my.out &&
         
         echo "Passed!"
         
         echo && echo
      done
   fi
   
elif test "$1" = "pedantic" && test -d $2
then
   for file in "$2"/*.my.py
   do
      echo "Being pedantic about.. $file:" &&
      
      echo "pyflakes:" &&
      pyflakes "$file" &&
      
      echo "pep8:" &&
      pep8 "$file" &&
      
      echo
   done
   
elif test "$1" = "clean" && test -d $2
then
   for file in "$2"/*.sh
   do
      echo "Deleting output files for.. $file"
      rm "$file".*.out
      
      if test "$3" = "all"
      then
         rm "$file".my.py
      fi
   done
fi
