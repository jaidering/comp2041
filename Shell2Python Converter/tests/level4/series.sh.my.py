#!/usr/bin/python2.7 -u
import subprocess
import os
import sys
start = 13
if int(int(len(sys.argv)-1)) > int(0):
    start = sys.argv[1]
i = 0
number = start
file = './tmp.numbers'
subprocess.call(['rm', '-f', str(file)])
while subprocess.call(['false']):
    if os.access(file, os.R_OK):
        if not subprocess.call(['fgrep', '-x', '-q', str(number), str(file)]):
            print 'Terminating', 'because', 'series', 'is', 'repeating'
            sys.exit(0)
    with open(file, 'a') as fileHandle4542: print >> fileHandle4542, number
    print i, number
    k = int(number) % int(2)
    if int(k) == int(1):
        number = int(7) * int(number) + 3
    else:
        number = int(number) / int(2)
    i = int(i) + int(1)
    if int(number) > int(100000000) or int(number) < int(-100000000):
        print 'Terminating', 'because', 'series', 'has', 'become', 'too', 'large'
        sys.exit(0)
subprocess.call(['rm', '-f', str(file)])
