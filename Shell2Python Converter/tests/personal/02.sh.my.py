#!/usr/bin/python2.7 -u
import subprocess
import glob
import os.path

subprocess.call(['ls', '-l'])

# testing:
#  intendation
#  test operator -e, -f, -d
#  variables
#  if, elif, else
for file in sorted(glob.glob("*")):
    print file
    for x in sorted(glob.glob("*")):
        if os.path.exists(x):  # an if after a do, annoying!
            for y in sorted(glob.glob("*")):
                if os.path.exists(y):
                    print 'BANANA!'
                elif os.path.isdir(y):
                    print 'APELSIN!'
                else:
                    print 'ANANAS!'

if 'ehlo' == 'helo':
    print 'What???'

subprocess.call(['ls', '-l'])  #this should be back at zero intendation
