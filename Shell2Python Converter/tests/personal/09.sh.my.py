#!/usr/bin/python2.7 -u
import subprocess

# testing &&
subprocess.call(['test', '-d', '/tmp/', '&&', 'echo', '-n', 'tmp exists, yay! ', '&&', 'echo', 'Also, echo works, yay!'])

#testing ||
subprocess.call(['test', '-f', '/tmp/thisProbablyDoesntExist', '||', 'echo', 'That random file did not exist!'])
