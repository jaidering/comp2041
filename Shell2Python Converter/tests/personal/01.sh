#!/bin/sh

# This is a comment! # and this is the same comment

# Testing simple echo with double quotes
echo "Hello with double quotes"

# Testing simple echo with double quotes and comment
echo "Hello with double quotes" # this is also a comment!

# Testing simple echo with single quotes
echo 'Hello with single quotes'

# Testing simple echo without quotes
echo Hello without quotes

# Mixing no quotes and quotes
echo Below this line should say 'echo hello', not "print hello":

# Testing reserved words in strings
echo "'echo hello'"

# Testing variable assignment
var1="Hello from a variable 1!"

# Testing echo of single variable
echo $var1

# Testing variable assignment CAPITAL
VAR1="Hello from a variable 1!"
# Testing echo of single variable CAPITAL
echo $VAR1

# Testing echo of two variables on the same line
aa="Hello from aa"
bb="Hello from bb"
echo $aa $bb

# Testing multiple variables on the same line
var1="Variable 1"
var2="Variable 2"
var3="Variable 3"
var4="Variable 4"
echo $var1 $var2 $var3
echo $var1 $var2 $var3 $var4

# variable in string
echo "aa $var2 aa"
echo "You supplied one argument!" $var2
echo $var2 "You supplied one argument!"

# Fake variable and apostrophe
echo $var1 "I'd like \$100.000 please" $var2

# echo -n
echo -n "There should be no new line here --->"
echo "<---- But there will be one here --->"

# testing multiple quotes on the same line
echo "Hello" "these are" 'all' "quotes!"

# should print, not execute
echo "echo '' > /tmp/afile"
echo "for f in *"

# blank line
echo
