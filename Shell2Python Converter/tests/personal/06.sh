#!/bin/sh

if test "ehlo" = "helo"
then
   echo "This should never happen."
fi

if test "helo" != "helo"
then
   echo "This should never happen."
fi

if test 1 -eq 2
then
   echo "This should never happen."
fi

if test 1 -ge 2
then
   echo "This should never happen."
fi

if test 1 -gt 2
then
   echo "This should never happen."
fi

if test 2 -le 1
then
   echo "This should never happen."
fi

if test 2 -lt 1
then
   echo "This should never happen."
fi

if test 1 -ne 1
then
   echo "This should never happen."
fi

if test ! 1 -eq 1
then
   echo "This should never happen."
fi

if test ! -f $0
then
   echo "This should never happen."
fi

if test ! -e $0
then
   echo "This should never happen."
fi

if test ! -d "/tmp/"
then
   echo "This should never happen."
fi
