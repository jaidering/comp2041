#!/usr/bin/python2.7 -u
import sys
import os.path

if 'ehlo' == 'helo':
    print 'This', 'should', 'never', 'happen.'

if 'helo' != 'helo':
    print 'This', 'should', 'never', 'happen.'

if int(1) == int(2):
    print 'This', 'should', 'never', 'happen.'

if int(1) >= int(2):
    print 'This', 'should', 'never', 'happen.'

if int(1) > int(2):
    print 'This', 'should', 'never', 'happen.'

if int(2) <= int(1):
    print 'This', 'should', 'never', 'happen.'

if int(2) < int(1):
    print 'This', 'should', 'never', 'happen.'

if int(1) != int(1):
    print 'This', 'should', 'never', 'happen.'

if not int(1) == int(1):
    print 'This', 'should', 'never', 'happen.'

if not os.path.exists(sys.argv[0]):
    print 'This', 'should', 'never', 'happen.'

if not os.path.exists(sys.argv[0]):
    print 'This', 'should', 'never', 'happen.'

if not os.path.isdir('/tmp/'):
    print 'This', 'should', 'never', 'happen.'
