#!/bin/sh

# writing to a file
temp_file="temp_file.$$";
echo "Hello world from a file!" > $temp_file

if test -f "$temp_file"
then
   cat "$temp_file"
   rm "$temp_file"
fi
