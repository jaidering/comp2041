#!/usr/bin/python2.7 -u
import sys

# This is a comment!  # and this is the same comment

# Testing simple echo with double quotes
print 'Hello', 'with', 'double', 'quotes'

# Testing simple echo with double quotes and comment
print 'Hello', 'with', 'double', 'quotes'  # this is also a comment!

# Testing simple echo with single quotes
print 'Hello with single quotes'

# Testing simple echo without quotes
print 'Hello', 'without', 'quotes'

# Mixing no quotes and quotes
print 'Below', 'this', 'line', 'should', 'say', 'echo hello,', 'not', 'print hello:'

# Testing reserved words in strings
print 'echo hello'

# Testing variable assignment
var1 = 'Hello from a variable 1!'

# Testing echo of single variable
print var1

# Testing variable assignment CAPITAL
VAR1 = 'Hello from a variable 1!'
# Testing echo of single variable CAPITAL
print VAR1

# Testing echo of two variables on the same line
aa = 'Hello from aa'
bb = 'Hello from bb'
print aa, bb

# Testing multiple variables on the same line
var1 = 'Variable 1'
var2 = 'Variable 2'
var3 = 'Variable 3'
var4 = 'Variable 4'
print var1, var2, var3
print var1, var2, var3, var4

# variable in string
print 'aa', var2, 'aa'
print 'You supplied one argument!', var2
print var2, 'You supplied one argument!'

# Fake variable and apostrophe
print var1, 'I\'d like $100.000 please', var2

# echo -n
sys.stdout.write('There should be no new line here --->')
print '<----', 'But', 'there', 'will', 'be', 'one', 'here', '--->'

# testing multiple quotes on the same line
print 'Hello', 'these are', 'all', 'quotes!'

# should print, not execute
print 'echo', '', '>', '/tmp/afile'
print 'for', 'f', 'in', '*'

# blank line
print 
