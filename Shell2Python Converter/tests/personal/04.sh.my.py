#!/usr/bin/python2.7 -u
import sys

# testing case

if int(len(sys.argv)-1) == 0:
    print 'You', 'forgot', 'to', 'supply', 'the', 'argument'

elif int(len(sys.argv)-1) == 1:
    print 'You supplied one argument!', sys.argv[1]

else:
    print 'You', 'supplied', 'too', 'many', 'arguments'


if sys.argv[1] == 'a':
    print sys.argv[1], 'looks', 'like', 'you', 'entered', 'a', 'a!'

elif sys.argv[1] == 'b':
    print sys.argv[1], 'looks', 'like', 'you', 'entered', 'a', 'b!'

elif sys.argv[1] == 'c':
    print sys.argv[1], 'looks', 'like', 'you', 'entered', 'a', 'c!'

elif sys.argv[1] == 'd':
    print sys.argv[1], 'looks', 'like', 'you', 'entered', 'a', 'd!'

else:
    print 'I', 'have', 'no', 'idea', 'what', 'you', 'entered..', 'What', 'is', 'a', sys.argv[1]


# FIXME: coult not be translated # *) echo "Multiple lines in a case statement, aaah! $1?"
print 'Multiple', 'lines', 'in', 'a', 'case', 'statement,', 'aaah!', sys.argv[1]
print 'Multiple', 'lines', 'in', 'a', 'case', 'statement,', 'aaah!', sys.argv[1]
print 'Multiple', 'lines', 'in', 'a', 'case', 'statement,', 'aaah!', sys.argv[1]
print 'Multiple lines in a case statement, aaah! $1?', ';;'
