#!/usr/bin/python2.7 -u
import os
import sys

print int(os.getpid())
print ' '.join(sys.argv[1:])
print int(len(sys.argv)-1)
print ' '.join(sys.argv[1:]), 'socks!'
print 'socks: ', sys.argv[0]

# special file names
temp_file = 'temp_file.' + str(int(len(sys.argv)-1))
print temp_file

temp_file = 'temp_file.' + str(int(os.getpid()))
print temp_file

temp_file = 'temp_file.' + sys.argv[1]
print temp_file

# python reserved names
print26537 = 1
print print26537

import26537 = 1
print import26537

print print26537, import26537
