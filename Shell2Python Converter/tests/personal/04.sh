#!/bin/sh

# testing case

case $# in
0) echo "You forgot to supply the argument" ;;
1) echo "You supplied one argument!" $1  ;;
*) echo "You supplied too many arguments" ;;
esac

case "$1" in
a) echo "$1 looks like you entered a a!" ;;
b) echo "$1 looks like you entered a b!" ;;
c) echo "$1 looks like you entered a c!" ;;
d) echo "$1 looks like you entered a d!" ;;
*) echo "I have no idea what you entered.. What is a $1?" ;;
esac

case "$1" in
*) echo "Multiple lines in a case statement, aaah! $1?"
   echo "Multiple lines in a case statement, aaah! $1?"
   echo "Multiple lines in a case statement, aaah! $1?"
   echo "Multiple lines in a case statement, aaah! $1?"
   echo "Multiple lines in a case statement, aaah! $1?" ;;
esac
