#!/bin/sh

ls -l

# testing:
#  intendation
#  test operator -e, -f, -d
#  variables
#  if, elif, else
for file in *
do
   echo "$file"
   for x in *
   do
      if test -e "$x" # an if after a do, annoying!
      then
         for y in *
         do
            if test -f "$y"
            then
               echo "BANANA!"
            elif test -d "$y"
            then
               echo "APELSIN!"
            else
               echo "ANANAS!"
            fi
         done
      fi
   done
done

if test "ehlo" = "helo"
then
   echo "What???"
fi

ls -l #this should be back at zero intendation
