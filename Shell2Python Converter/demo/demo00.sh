#!/bin/sh

# Prints what is in your /tmp/ at the current date

# Usage: ./app
# by James French

date=`date` # sweet assignment bro

print="At $date this in the /tmp/ dir:"

if test -d "/tmp/"
then
   for use in /tmp/*
   do
      echo $print $use
   done
fi
