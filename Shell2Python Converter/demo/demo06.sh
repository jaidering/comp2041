#!/bin/sh

# Sleeps in increments of x

# Usage: ./app x
# by James French

increments=$1
timeToSleep=0

while true
do
   echo "Sleeping for $timeToSleep seconds"
   sleep $timeToSleep
   timeToSleep=`expr $timeToSleep + $increments`
done
