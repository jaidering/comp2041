#!/bin/sh

# Get file names from working dir, put in file and then cat

# Usage: ./app
# by James French

testingFile="/tmp/theTestingFile.$$";

if test -d "$testingFile"
then
   exit 1
fi

# get names of files in current directory
for f in *
do
   echo "$f" >> "$testingFile"
done

# print file back
echo "Printing contents of file" "$testingFile"
cat "$testingFile"

# remove file
rm "$testingFile"
