#!/usr/bin/python2.7 -u
from subprocess import Popen, PIPE, STDOUT
import subprocess
import os

# Writes my name out to a file
# then uses that file as input to
# demo04.sh, which reads from stdin,
# and also takes a command line arg.

# Usage: ./app
# by James French

tempFile = '/tmp/tempDemoFile.' + str(int(os.getpid()))

with open(tempFile, 'a') as fileHandle5469: print >> fileHandle5469, 'James'

readFileHandle5469 = open(tempFile, 'r').read()
p = Popen(['./demo04.sh', 'hello'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
pStdout = p.communicate(input=readFileHandle5469)[0]
print str(pStdout).rstrip('\n')

subprocess.call(['rm', str(tempFile)])
