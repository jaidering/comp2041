#!/bin/sh

# Writes my name out to a file
# then uses that file as input to
# demo04.sh, which reads from stdin,
# and also takes a command line arg.

# Usage: ./app
# by James French

tempFile="/tmp/tempDemoFile.$$"

echo "James" >> "$tempFile"
./demo04.sh hello < "$tempFile"

rm "$tempFile"