#!/bin/sh

# This program really doesn't do anything
# but I'm running out of ideas and time.
# At least it translates correctly.

# Usage: ./app
# by James French

ls -l

for file in *
do
   echo "$file"
   for x in *
   do
      if test -e "$x"
      then
         for y in *
         do
            if test -f "$y"
            then
               echo "BANANA!"
            elif test -d "$y"
            then
               echo "APELSIN!"
            else
               echo "ANANAS!"
            fi
         done
      fi
   done
done

if test "ehlo" = "helo"
then
   echo "What???"
fi
