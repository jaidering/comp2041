#!/bin/sh

# Print out arguments passed

# Usage: ./app [arguments] ...
# by James French

echo 'Things in the list, including your arguments:'
for f in one two three "$@"
do
   echo "$f"
done

echo 'Your arguments:'
for f in one two three "$@"
do
   echo "$f"
done
