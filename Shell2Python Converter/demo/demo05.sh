#!/bin/sh

# Useless program that calls itself recursively
# while in every instance a random value is greater
# than max

# Usage: ./app
# by James French

max=2000
i=$RANDOM

if test -f $0
then
   if test "$i" -gt "$max"
   then
     echo "i = $i"
     $0
   fi
fi
