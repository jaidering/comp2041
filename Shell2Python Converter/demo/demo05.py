#!/usr/bin/python2.7 -u
import subprocess
import sys
import os.path
import random

# Useless program that calls itself recursively
# while in every instance a random value is greater
# than max

# Usage: ./app
# by James French

max = 2000
i = random.randint(0, 32767)

if os.path.exists(sys.argv[0]):
    if int(i) > int(max):
        print 'i', '=', i
        subprocess.call(sys.argv[0])
