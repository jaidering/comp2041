#!/bin/sh

# Download and search an url for stuff

# Usage: ./app pattern url
# by James French

tempFile="/tmp/wgetFile.$$"
pattern="$1"
url="$2"

wget -q -O "$tempFile" "$url"
egrep "$pattern" "$tempFile"

rm "$tempFile"
