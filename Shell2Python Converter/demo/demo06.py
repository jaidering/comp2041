#!/usr/bin/python2.7 -u
import subprocess
import time
import sys

# Useless program that calls itself recursively
# while in every instance a random value is greater
# than max

# Usage: ./app
# by James French

increments = sys.argv[1]
timeToSleep = 0

while subprocess.call(['false']):
    print 'Sleeping', 'for', timeToSleep, 'seconds'
    time.sleep(timeToSleep)
    timeToSleep = int(timeToSleep) + int(increments)
