#!/bin/sh

# Interactive friend for you

# Usage: ./app word
# by James French

echo 'Please enter a name: '
read name

for a in "$@"
do
   case $a in
      hello) echo "Well hello to you to dear sir and/or madam.. $name" ;;
      "please die") echo "Might as well. Nothing matters anyway." ;;
      hungry?) echo "Why yes I would not mind a bite actually.. You buying?" ;;
      bye) echo "Bye $name fare thee well." ;;
      *) echo "I dont understand what you just said.. Sorry!" ;;
   esac
done
