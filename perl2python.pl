#!/usr/bin/perl -w

use strict;

our @code = ();
our %dependencies = ();
our %variables = ();
our $curIndent = 0;
our $tabWidth = 4;

while (my $line = <>) {
   #strip whitespace at beginning of line
   $line =~ s/^\s*//;
   
   #empty lines carry over
   if ($line =~ /^$/) {
      push @code, (" " x $curIndent)."\n";
      next;
   }
   
   if ($line =~ /^use/) {
      next;
   } elsif ($line =~ /^print/) {
      parsePrint($line);
   } elsif ($line =~ /^\$/) {
      parseVar($line);
   } elsif ($line =~ /^\@/) {
      parseArr($line);
   } elsif ($line =~ /^if/) {
      parseIf($line, "if");
   } elsif ($line =~ /^else/) {
      push @code, (" " x $curIndent)."else:\n";
      $curIndent += $tabWidth;
   } elsif ($line =~ /^elsif/) {
      parseIf($line, "elsif");
   } elsif ($line =~ /^while/) {
      parseWhile($line);
   } elsif ($line =~ /^last/) {
      push @code, (" " x $curIndent)."break\n";
   } elsif ($line =~ /^foreach/) {
      parseForeach($line);
   } elsif ($line =~ /^\}\s*(\S*)(.*$)/) {
      $curIndent -= $tabWidth;
      if ($1 eq "elsif") {
         parseIf($1.$2, "elsif");
      } elsif ($1 eq "else") {
         push @code, (" " x $curIndent)."else:\n";
         $curIndent += $tabWidth;
      }
   } elsif ($line =~ /^chomp/) {
      parseChomp($line);
   } elsif ($line =~ /^#/) {
      push @code, (" " x $curIndent)."$line\n";
   }
}

# foreach my $key (keys %variables) {
#    print "$key is of type $variables{$key}\n";
# }

print "#!/usr/bin/python\n\n";
foreach my $dependency (keys %dependencies) {
   print "import $dependency\n";
}
print "\n";
foreach my $line (@code) {
   print $line;
}

sub parseChomp {
   my ($line) = @_;

   $line =~ /^chomp\s*\(?([\$\@\w]*)\)?/;
   my $chompTarget = $1;
   $chompTarget =~ s/^\$//;

   push @code, (" " x $curIndent)."$chompTarget = $chompTarget.rstrip('\\n')\n";
}

sub parseForeach {
   my ($line) = @_;

   $line =~ /^foreach\s+\$(\w+)\s*\((.+)\)/;
   my $variable = $1;
   my $iterative = $2;

   if ($iterative =~ /(.+)\.\.(.+)/) {
      my $from = parseExpr($1);
      my $to = parseExpr($2."+1");
      $iterative = "range($from, $to)";
   } else {
      $iterative = parseExpr($iterative);
   }

   push @code, (" " x $curIndent)."for $variable in $iterative:\n";
   $curIndent += $tabWidth;
}

sub parseIf {
   my ($line, $ifType) = @_;

   $line =~ /^$ifType\s*\((.*)\)/;
   my $condition = parseExpr($1);

   if ($ifType eq "elsif") {
      $ifType = "elif";
   }

   push @code, (" " x $curIndent)."$ifType ".$condition.":\n";
   $curIndent += $tabWidth;
}

sub parseWhile {
   my ($line) = @_;

   $line =~ /^while\s*\((.*)\)/;
   my $condition = $1;

   my $conditionStripped = $condition;
   $conditionStripped =~ s/".*?"//g;
   $conditionStripped =~ s/'.*?'//g;
   if ($conditionStripped =~ /(?:\$(\w+)\s*=\s*)?<(.*?)>/) {
      my $varName = $1;
      my $fileInput = $2;
      if ($fileInput eq "STDIN") {
         $fileInput = "sys.stdin";
         $dependencies{'sys'}++;
      } elsif ($fileInput eq "") {
         $fileInput = "fileinput.input()";
         $dependencies{'fileinput'}++;
      }
      push @code, (" " x $curIndent)."for $varName in $fileInput:\n";
   } else {
      push @code, (" " x $curIndent)."while ".parseExpr($condition).":\n";
   }

   $curIndent += $tabWidth;
}

sub parseVar {
   my ($line) = @_;
   
   $line =~ /^\$(\w+)\s*([-.*\/|^+=~%]+)\s*(.*);\s*/;
   my $leftVar = $1;
   my $operator = $2;
   my $rightExpr = $3;

   if ($rightExpr =~ /<(.*?)>/) {
      my $input = $1;
      if ($input =~ /\s*STDIN\s*/) {
         $input = "sys.stdin";
         $dependencies{'sys'}++;
      }
      $input =~ s/^\s*//;
      $input =~ s/\s*$//;
      if ($variables{$leftVar}) {
         if ($variables{$leftVar}{'type'} ne "string") {
            $rightExpr = "$variables{$leftVar}{'type'}($input.readline())";
         } else {
            $rightExpr = "$input.readline()";
         }
      } else {
         $variables{$leftVar}{'type'} = "undefined-string";
         $variables{$leftVar}{'declaration'} = scalar(@code);
         $rightExpr = "$input.readline()";
      }

      push @code, (" " x $curIndent)."$leftVar $operator $rightExpr\n";
   } elsif ($operator eq "=~") {
      $dependencies{'re'}++;

      $rightExpr =~ /^\w*(\W)/;
      my $rSep = $1;              #regexSeperator
      $rSep = "\\".$rSep;         #escaping it
      $rightExpr =~ /^(\w?)$rSep(.+?)$rSep(.*?)$rSep(.*?)/;
      my $regexType = $1;
      my $regex = $2;
      my $repl = $3;
      my $flags = $4;

      if ($regexType eq "s") {
         my $global = 1;
         $global = 0 if $flags =~ /g/;
         my $case = "";
         $case = "re.IGNORECASE" if $flags =~ /i/;
         my $finalCode = "$leftVar = re.sub(r'$regex', r'$repl', $leftVar";
         $finalCode .= ", $global" if !$global;
         if ($case) {
            $finalCode .= ", $case)\n";
         } else {
            $finalCode .= ")\n";
         }
         push @code, (" " x $curIndent).$finalCode;
      }
   } elsif ($operator eq "++") {
      push @code, (" " x $curIndent)."$leftVar += 1\n";
   } elsif ($operator eq "--") {
      push @code, (" " x $curIndent)."$leftVar -= 1\n";
   } else {
      my $type = "undefined";
      $rightExpr = parseExpr($rightExpr, $type);
      if (!$variables{$leftVar}) {
         $variables{$leftVar}{'type'} = $type;
         $variables{$leftVar}{'declaration'} = scalar(@code);
      }
      push @code, (" " x $curIndent)."$leftVar $operator $rightExpr\n";
   }
}

sub parseArr {
   my ($line) = @_;
   
   $line =~ /^\@(\w+)\s*([-.*\/+=%]+)\s*(.+);/;
   my $leftVar = $1;
   my $operator = $2;
   my $rightExpr = $3;
   $rightExpr =~ s/\(/[/;
   $rightExpr =~ s/\)/]/;
   
   push @code, (" " x $curIndent)."$leftVar $operator $rightExpr\n";
}

sub parseExpr {
   my @expr = split //, $_[0];
   my $exprType = $_[1];          #optional passed by reference
   my @tokens = ();
   
   #note: the $i-- is to move it back one index so that
   #other if statement blocks have a chance to analyse that character
   for (my $i = 0; $i < scalar(@expr); $i++) {
      if ($expr[$i] =~ /["']/) {
         my $startQuote = $expr[$i];
         $i++;
         my $curStr = "";
         while (!($expr[$i] eq $startQuote && $expr[$i-1] ne "\\")) {
            #extract the relevant part of argument between quotes
            $curStr .= $expr[$i];
            $i++;
         }
         $curStr = convertString($curStr, $startQuote);
         push @tokens, {'value' => $curStr, 'type' => "string"};
      } elsif ($expr[$i] =~ /[0-9]/) {
         my $curNum = "";
         my $type = "int";
         while ($i < scalar(@expr) && $expr[$i] =~ /[0-9.]/) {
            if ($expr[$i] eq '.') {
               $type = "float";
            }
            $curNum .= $expr[$i];
            $i++;
         }
         $i--;
         push @tokens, {'value' => $curNum, 'type' => $type};
      } elsif ($expr[$i] =~ /[a-zA-Z]/) {
         my $curFunc = "";
         my $type = "function";
         while ($i < scalar(@expr) && $expr[$i] =~ /[a-zA-Z]/) {
            $curFunc .= $expr[$i];
            $i++;
         }
         $i--;

         if ($curFunc eq "eq") {
            $curFunc = "==";
            $type = "op-string";
         } elsif ($curFunc eq "ne") {
            $curFunc = "!=";
            $type = "op-string";
         } elsif ($curFunc eq "x") {
            $curFunc = "*";
            $type = "op-string";
         }
         push @tokens, {'value' => $curFunc, 'type' => $type};
      } elsif ($expr[$i] eq '$') {
         my $curVar = "";
         my $type = "";
         $i++;
         if ($expr[$i] eq '#') {
            $type = "var-int";
            $i++;
         } else {
            $type = "var";
         }
         while ($i < scalar(@expr) && $expr[$i] =~ /[\w\[\]\$]/) {
            $curVar .= $expr[$i];
            $i++;
         }
         $i--;
         
         $curVar =~ s/\$//g;

         if ($curVar eq "ARGV") {
            $curVar = "sys.argv[1:]";
            $dependencies{'sys'}++;
         }
         if ($type eq "var-int") {
            $curVar = "(len($curVar) - 1)";
         }
         push @tokens, {'value' => $curVar, 'type' => $type};
      } elsif ($expr[$i] eq '@') {
         my $curVar = "";
         $i++;
         while ($i < scalar(@expr) && $expr[$i] =~ /\w/) {
            $curVar .= $expr[$i];
            $i++;
         }
         $i--;
         if ($curVar eq "ARGV") {
            $dependencies{'sys'}++;
            $curVar = "sys.argv[1:]";
         }
         push @tokens, {'value' => $curVar, 'type' => "array"};
      } elsif ($expr[$i] =~ /[\(\)]/) {
         push @tokens, {'value' => $expr[$i], 'type' => "parentheses"};
      } elsif ($expr[$i] eq ",") {
         push @tokens, {'value' => ",", 'type' => "comma"};
      } elsif ($expr[$i] =~ /[-.*<>|^~\/+=%]/) {
         my $curOp = "";
         while ($i < scalar(@expr) && $expr[$i] =~ /[-.*<>|^~\/+=%]/) {
            $curOp .= $expr[$i];
            $i++;
         }
         $i--;

         if ($curOp eq ".") {
            push @tokens, {'value' => "+", 'type' => "op-string"};
         } else {
            push @tokens, {'value' => $curOp, 'type' => "op"};
         }
      }
   }

   # foreach my $token (@tokens) {
   #    if ($token->{'value'} eq "ARGV" && $token->{'type'} eq "array") {
   #       print "YES~~~!!!\n";
   #       $dependencies{'sys'}++;
   #       $token->{'value'} = "sys.argv";
   #    }
   # }

   for (my $i = 0; $i < scalar(@tokens); $i++) {
      my $token = $tokens[$i];
      if ($token->{'type'} eq "function") {
         if ($token->{'value'} eq "join") {
            my $joinFuncIndex = $i;
            my @joinStrings = ();
            my $arrayToApply = "";

            $i += 2;
            $token = $tokens[$i];
            while ($token->{'value'} ne ",") {
               push @joinStrings, $token->{'value'};

               $i++;
               $token = $tokens[$i];
            }
            $i++;
            $token = $tokens[$i];
            ($token->{'type'} eq "array") or die "Does not have array for join??\n";
            $arrayToApply = $token->{'value'};

            my $joinString = "";
            if (scalar(@joinStrings) > 1) {
               $joinString = "(".join('', @joinStrings).").join($arrayToApply)";
            } else {
               $joinString = join('', @joinStrings).".join($arrayToApply)";
            }

            #now replace the original things with the completed string
            $tokens[$joinFuncIndex]->{'value'} = $joinString;
            for (my $j = $joinFuncIndex+1; $j <= $i+1; $j++) {
               splice(@tokens, $joinFuncIndex+1, 1);
            }
            $i = $joinFuncIndex + 1;
         }
      }
   }

   if ($exprType) {
      foreach my $token (@tokens) {
         if ($token->{'type'} eq "string" ||
             $token->{'type'} eq "int" ||
             $token->{'type'} eq "float" ||
             $token->{'type'} eq "array") {
            $_[1] = $token->{'type'};
            last;
         }
      }
   }
   
   for (my $i = 0; $i < scalar(@tokens); $i++) {
      if ($tokens[$i]->{'type'} =~ /op/) {
         my $leftToken = $tokens[$i-1];
         my $rightToken = $tokens[$i+1];
         
         if ($tokens[$i]->{'type'} eq "op-string") {
            ($leftToken->{'value'}, $leftToken->{'type'}) = castToType($leftToken->{'value'}, $leftToken->{'type'}, "string");
            ($rightToken->{'value'}, $rightToken->{'type'}) = castToType($rightToken->{'value'}, $rightToken->{'type'}, "string");
         }
         elsif ($tokens[$i]->{'value'} =~ /[*+-=<>\|&\/]/) {
            ($leftToken->{'value'}, $leftToken->{'type'}) = castToType($leftToken->{'value'}, $leftToken->{'type'}, "intorfloat");
            ($rightToken->{'value'}, $rightToken->{'type'}) = castToType($rightToken->{'value'}, $rightToken->{'type'}, "intorfloat");
         }
      }
   }
   
   #print "TOKENS COMBINED ARE ", join(' ', map({my %temp = %{$_}; $temp{'value'}} @tokens)), "\n";
   my $combinedExpr = join(' ', map({my %temp = %{$_}; $temp{'value'}} @tokens));
   
   #compress brackets...just aesthetics
   $combinedExpr =~ s/\( /(/g;
   $combinedExpr =~ s/ \)/)/g;

   return $combinedExpr;
}

sub castToType {
   my ($value, $type, $expectedType) = @_;
   
   if ($type eq "var") {
      if ($variables{$value}) {
         if ($variables{$value}{'type'} =~ "undefined") {
            if ($expectedType eq "string" && $variables{$value}{'type'} eq "undefined-string") {
               $variables{$value}{'type'} = "string";
               $type = "string";
            } else {
               $type = $variables{$value}{'type'};
               
               my $lineDeclared = $code[$variables{$value}{'declaration'}];
               $lineDeclared =~ /^\s*(\w+)\s*([-.*\/|^+=~%]+)\s*(.*)\s*$/;
               my $rightExpr = $3;
               $rightExpr =~ s/\(/\\(/g;
               $rightExpr =~ s/\)/\\)/g;
               my $newType = "str";
               $newType = "float" if $expectedType eq "intorfloat";
               $lineDeclared =~ s/($rightExpr)/$newType($1)/;
               $code[$variables{$value}{'declaration'}] = $lineDeclared;
               
               $variables{$value}{'type'} = $newType;
            }
         } else {
            $type = $variables{$value}{'type'};
         }
      } else {
         $type = "undefined";
      }
   } elsif ($type eq "var-int") {
      $type = "int";
   }
   
   if ($expectedType eq "floatorint") {
      if ($type ne "float" && $type ne "int") {
         return ("float($value)", "float");
      }
   }
   if ($expectedType eq "string") {
      if ($type ne "string") {
         return ("str($value)", "string");
      }
   }
   return ($value, $type);
}

#combination of joinString and parseString.
#given a single string it returns a complete
#python formatted string
sub convertString {
   my $string = $_[0];
   my $startQuote = $_[1];
   
   my @argFormat = ();
   my $result = "'".parseString($string, \@argFormat, $startQuote)."'";
   
   if (@argFormat) {
      $result .= " % (";
      $result .= join(', ', @argFormat);
      $result .= ")\n";
   }
    
   return $result;
}

#given a formatted string with an array of
#arguments in order, combines them together
sub joinString {
   my $result = $_[0];
   my @argFormat = $_[1];
   
   $result = "'".$result."'";
   if (@argFormat) {
      $result .= " % (";
      $result .= join(', ', @argFormat);
      $result .= ")\n";
   }
   
   return $result;
} 

#given a string without the quotes it puts
#variables into an array via reference
#and returns the original string with variables
#replaced with %s
sub parseString {
   my $curArg = $_[0];
   my $formatRef = $_[1];
   my $startQuote = $_[2];
   
   if ($startQuote eq '"') {
      #in double quotes we replace variables
      #push @{$formatRef}, $curArg =~ /(?<=\$)\w+/g;
      my @variables = $curArg =~ /[\$@]\w+(?:\[.+?\])?/g;

      foreach my $variable (@variables) {
         if ($variable =~ /^\$/) {
            #scalar variable
            my $escapedRegex = $variable;
            $escapedRegex =~ s/\$/\\\$/g;
            $escapedRegex =~ s/\[/\\[/g;
            $escapedRegex =~ s/\]/\\]/g;

            $curArg =~ s/$escapedRegex/%s/;

            $variable =~ s/\$//g;
            $variable =~ s/\bARGV\b/sys.argv/g;
            push @{$formatRef}, $variable;
         } elsif ($variable =~ /^\@/) {
            #array
            $variable =~ s/^\@//;
            if ($variable eq "ARGV") {
               $dependencies{'sys'}++;
               push @{$formatRef}, "' '.join(sys.argv[1:])";
            } else {
               push @{$formatRef}, "' '.join($variable)";
            }
            $curArg =~ s/\@\w+/%s/g;
         }
      }
      
      $curArg =~ s/'/\\'/g;
   }
   if ($startQuote eq '\'') {
      #in single quotes backslashes are literal
      $curArg =~ s/\\/\\\\/g;
   }
   
   return $curArg;
}

#parses a print statement
sub parsePrint {
   my ($line) = @_;
   
   #remove print statement
   $line =~ s/^print\s*//;

   #remove brackets if any
   $line =~ s/^\(//;
   $line =~ s/\)\s*;?\s*$//;
   
   #split line into array of chars and iterate through it
   my @lineAsChars = split //, $line;

   my @printArgs = ();     #each argument from the print statement
   my $curArg = "";
   my $inQuote = "";    #not in a quote, ignore commas that are in quotes
   my $inBracket = 0;   #not in a bracket, ignore commas that are in brackets
   for (my $i = 0; $i < scalar(@lineAsChars); $i++) {
      if ($lineAsChars[$i] eq "," && !$inQuote && !$inBracket) {
         push @printArgs, $curArg;
         $curArg = "";
         $i++;
      }
      if ($lineAsChars[$i] =~ /[\(\)]/ && !$inQuote) {
         $inBracket++ if $lineAsChars[$i] eq "(";
         $inBracket-- if $lineAsChars[$i] eq ")";
      }
      if ($lineAsChars[$i] =~ /['"]/ && $lineAsChars[$i-1] ne "\\") {
         if ($lineAsChars[$i] eq $inQuote) {
            $inQuote = "";       #found closing quote, no longer in quote
         } elsif ($inQuote eq "") {
            $inQuote = $lineAsChars[$i];  #remember what type of quote it's in
         }
      }
      $curArg .= $lineAsChars[$i];
   }
   push @printArgs, $curArg;     #push the last arg on
   
   #strip whitespace before and after args
   foreach my $arg (@printArgs) {
      $arg =~ s/^\s*//;
      $arg =~ s/[\s;]*$//;
   }

   # print "Printing print args: ";
   # print "@printArgs\n";

   #array which stores the variables at the end
   my @argFormat = ();
   #the formatted string part
   my $argString = "";

   #now parse each argument depending on what type it is
   foreach my $arg (@printArgs) {
      if ($arg =~ /^(['"]).*\1$/) {
         #if its a string then pass to parseString

         #get the quote type
         my $startQuote = $1;

         #strip the quotes
         $arg =~ s/^['"]//;
         $arg =~ s/['"]$//;

         $arg = parseString($arg, \@argFormat, $startQuote);
         $argString .= $arg;
      } else {
         #it's (probably?) an expression, parseExpr it
         $arg = parseExpr($arg);

         push @argFormat, $arg;
         $argString .= "%s";
      }
   }
   
   if (scalar(@argFormat) == 1 &&
       $argFormat[0] =~ /^\w+$/ &&
       $argString =~ /^%s(?:\\n)?$/) {
      $argString =~ s/%s/$argFormat[0]/;
      pop @argFormat;
   } else {
      $argString = "'".$argString."'";
   }
   
   #combine argString and argFormat into a full
   #python-compatible print statement
   my $code = " " x $curIndent;
   if ($argString =~ s/([^\\])\\n$/$1/) {
      $code .= "print($argString";
   } else {
      $code .= "sys.stdout.write($argString";
      $dependencies{'sys'}++;
   }
   
   #only have the %(arg,arg) if there actually are args
   if (@argFormat) {
      $code .= " % (";
      $code .= join(', ', @argFormat);
      $code .= "))\n";
   } else {
      $code .= ")\n";
   }
   
   #push onto @code and we're done
   push @code, $code;
}